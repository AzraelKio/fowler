import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {
    @Test
    public void getDaysRented() {
        int rentedDays = 10;
        Rental rental = new Rental(null, rentedDays);

        assertEquals(rentedDays,rental.getDaysRented());
    }

    @Test
    public void getMovie() {
        Movie m1 = new Movie("Test",1);
        Rental rental = new Rental(m1,10);

        assertEquals(m1,rental.getMovie());
    }
}