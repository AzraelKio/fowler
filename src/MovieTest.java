import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {
    @Test
    public void getTitle() {
        String title = "Test";
        int priceCode = 2;
        Movie movie = new Movie(title, priceCode);

        assertEquals(title, movie.getTitle());
    }
}