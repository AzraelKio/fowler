import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    @Test
    public void testStatement(){
        String result;

        Movie m1 = new Movie("movie1", 1);
        Movie m2 = new Movie("movie2", 2);

        Rental r1 = new Rental(m1, 10);
        Rental r2 = new Rental(m2, 5);

        Customer c1 = new Customer("joe");

        c1.addRental(r1);
        c1.addRental(r2);

        result = "Rental Record for joe\n" +
                "\tTitle\t\tDays\tAmount\n" +
                "\tmovie1\t\t10\t30.0\n" +
                "\tmovie2\t\t5\t3.0\n" +
                "Amount owed is 33.0\n" +
                "You earned 2 frequent renter points";

        assertEquals( result , c1.statement()) ;
    }

    @Test
    public void getName() {
        String customerName = "Test Name";
        Customer c1 = new Customer(customerName);
        assertEquals(customerName, c1.getName());
    }
}